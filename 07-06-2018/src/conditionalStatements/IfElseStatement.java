package conditionalStatements;

public class IfElseStatement 
{
	
	public static void getAgeValidation()
	{
		/*
		 * if-statement syntax:
		 * 
		 * initialization;
		 * 
		 * if(condition/expression)
		 * {
		 * 		if body
		 * }
		 * else
		 * {
		 * 		else body
		 * }
		 * 
		 */
		int age = 12;
		
		if(age >= 18)
		{
			System.out.println("eligible to cast the vote bcz age is "+age);
		}
		else
		{
			System.out.println("not eligible to cast the vote bcz age is "+age);
		}
		
	}

	public static void main(String[] args)
	{
		
		getAgeValidation();
	}

}
