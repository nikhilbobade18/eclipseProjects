package conditionalStatements;

public class IfStatement 
{
	
	public static void getAgeValidation()
	{
		/*
		 * if-statement syntax:
		 * 
		 * initialization;
		 * 
		 * if(condition/expression)
		 * {
		 * 		if body
		 * }
		 * 
		 */
		int age = 12;
		
		if(age >= 18)
		{
			System.out.println("eligible to cast the vote bcz age is "+age);
		}
		
	}

	public static void main(String[] args)
	{
		
		getAgeValidation();
	}

}
