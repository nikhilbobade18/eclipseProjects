package conditionalStatements;

public class NestedIfElsestatement
{

	public static void main(String[] args)
	{
		
		/*
		 * if-statement syntax:
		 * 
		 * initialization;
		 * 
		 * if(condition/expression)
		 * {
		 * 		//if body
		 * 		if(condition/expression)
		 * 		{
		 * 
		 * 		}
		 * 		else
		 * 		{
		 * 
		 * 		}
		 * 		
		 * }
		 * else
		 * {
		 * 		//else body
		 * 		if(condition/expression)
		 * 		{
		 * 
		 * 		}
		 * 		else
		 * 		{
		 * 
		 * 		}
		 * }
		 * 
		 */
		
		int age = 2;
		
		if(age >= 18)
		{
			
			System.out.println("eligible to cast the vote bcz age is "+age);
			
			int a = 7, b = 4;
			
			if(a >= b)
			{
			
				System.out.println("a value: "+a);

			}
			else
			{
				System.out.println("b value: "+b);
			}
			
		}
		else
		{
			System.out.println("not eligible to cast the vote bcz age is "+age);
		
			int a = 3, b = 4;
			
			if(a >= b)
			{
			
				System.out.println("a value: "+a);

			}
			else
			{
				System.out.println("b value: "+b);
			}
		}
		
	}

}
