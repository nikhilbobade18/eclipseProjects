package loops;

public class DoWhile 
{

	public static void main(String[] args) 
	{
		
		/*
		 * 
		 * initialization;
		 * 
		 * do
		 * {
		 * 	
		 * 		do body
		 * 		inc/dec statement;
		 * 
		 * }
		 * while(condition/expression);
		 */
		
		int a = 0;
		
		do
		{
			System.out.println(a);
			a++;
			
		}
		while(a <= 5);
		
	}

}
