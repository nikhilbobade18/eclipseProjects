package loops;

public class ForLoop 
{

	public static void main(String[] args) 
	{
		
		/*
		 * for loop syntax: 
		 * 
		 * for(initialization; condition/expression; inc/dec statement)
		 * {
		 * 		
		 * 		//for loop body
		 * 
		 * }
		 * 
		 * for each loop syntax:
		 * 
		 * for(datatype variable: variable)
		 * {
		 * 		
		 * 		for each body
		 * 	
		 * }
		 * 
		 *  
		 */
		//for loop example
		for(int i = 1; i <= 10; i++)
		{
			System.out.println(i);
		}
		
		
		//for each
		
		int values[] = {1, 23, 321, 3425, 44, 2345, 985,0, 32};
		
		for(int i: values)
		{
			//System.out.println(i);
		}
		
	}

}
