package loops;

public class While 
{

	public static void main(String[] args) 
	{
		
		/*
		 * 
		 * initialization;
		 * 
		 * while(condition/expression)
		 * {
		 * 	
		 * 		while body
		 * 		inc/dec statement;
		 * 
		 * }
		 */
		
		int a = 0;
		
		while(a <= 5)
		{
			System.out.println(a);
			a++;
			
		}
	}

}
