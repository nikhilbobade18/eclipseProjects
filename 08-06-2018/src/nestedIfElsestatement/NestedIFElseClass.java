package nestedIfElsestatement;

public class NestedIFElseClass 
{

	public static void main(String[] args) 
	{
		
		/*
		 * else if statement syntax:
		 * 
		 * initialization;
		 * 
		 * if(condition/expression)
		 * {
		 * 		if body
		 * }
		 * else if(condition/expression)
		 * {
		 * 		else if body
		 * }
		 * else if(condition/expression)
		 * {
		 * 		else if body
		 * }
		 * else if(condition/expression)
		 * {
		 * 		else if body
		 * }
		 * else
		 * {
		 * 		else body
		 * }
		 * 
		 */
		
		
		float pctg = 32;
		
		if(pctg >= 70)
		{
			System.out.println("distintion");
		}
		
		else if(pctg >= 60 && pctg < 70)
		{
			System.out.println("first class");
		}
		
		else if(pctg >= 50 && pctg < 60)
		{
			System.out.println("second class");
		}
		
		else if(pctg >= 40 && pctg < 50)
		{
			System.out.println("third class");
		}
		else if(pctg >= 35 && pctg < 40)
		
		{
			System.out.println("pass");
		}
		
		else
		{
			System.out.println("fail........!");
		}
	}

}
