package switchStatement;

class SwitchCase 
{
	

	public static void main(String[] args) 
	{
		
		/*
		 * initialization;
		 * 
		 * switch(condition/expression)
		 * {
		 * 	
		 * 		case 1:
		 * 		statement1;
		 * 		break;
		 * 
		 * 		case 2:
		 * 		statement2;
		 * 		break;
		 * 
		 * 		case 3:
		 * 		statement3;
		 * 		break;
		 * 
		 * 		.
		 * 		.
		 * 
		 * 		.
		 * 		case n:
		 * 		statement n;
		 * 		break;
		 * 
		 * 		
		 * 
		 * 		default:
		 * 		statement;
		 * 
		 * }
		 * 
		 * 		
		 */
		
		String color = "Light Green";
		
		switch(color)
		{
			
			case "Red":
				System.out.println("Red color...!");
				break;
		
				
			case "Blue":
				System.out.println("Blue color...!");
				break;
				
				
			case "Yellow":
				System.out.println("Yellow color...!");
				break;
				
			case "Pink":
				System.out.println("Pink color...!");
				break;
				
				
			case "Black":
				System.out.println("Black color...!");
				break;
				
				
			case "Green":
				System.out.println("Green color...!");
				break;
				
			default:
				System.out.println(color+" color is not available");
		
		
		}
		
		
	}

}
