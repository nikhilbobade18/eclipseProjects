package inheritanceConceptNCAC;

//The type Employee must implement the inherited 
//abstract method Employee1.get()

public class Employee extends Employee1
{
	int id;
	public Employee()
	{
		id = 5;
	}
	public void getId()
	{
		System.out.println(id);
	}
	
	public static void main(String[] args) 
	{
		
		Employee e1 = new Employee();
		
		e1.get();
		//Cannot instantiate the type Employee1
		//Employee1 e12 = new Employee1();
		
	}
	/*
	 * Multiple markers at this line
	- The return type is incompatible with 
	 Employee1.get()
	- implements 
	 inheritanceConceptNCAC.Employee1.get
	 */
	//public int get()
	
	public void get() 
	{
		System.out.println(id);
	}

}
