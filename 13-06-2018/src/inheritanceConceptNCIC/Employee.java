package inheritanceConceptNCIC;

/*
 * Multiple markers at this line
	- The type Employee must implement the 
	inherited abstract method 
	 Employee22.getInfo1()
	- The type Employee must implement 
	the inherited abstract method 
	 Employee2.getInfo()
 */

public class Employee implements Employee2
{
	int id;

	public Employee()
	{
		id = 5;
	}
	public void getId()
	{
		System.out.println(id);
	}
	
	public static void main(String[] args) 
	{
		
		Employee e = new Employee();
		e.getInfo();
		
	}
	
	public void getInfo1() 
	{
		System.out.println(a1);
		
	}

	public void getInfo() 
	{
		System.out.println(id);
		System.out.println(a12);
	}

}
