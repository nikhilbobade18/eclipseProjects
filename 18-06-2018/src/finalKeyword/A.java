package finalKeyword;


//public final class A 
public class A 
{
	
	//final int id = 2233;
	
	int id = 2233;
	
	static String name = "A";
	
	/*//instance initializer block
	{
		id = 5566;
	}
	*/
	static
	{
		name = "C";
	}
	
	public A() 
	{
		//The final field A.id cannot be assigned
		id = 1122;
	}
	
	
	//public final void getId()
	public void getId()
	{
		System.out.println(id);
	}
	
	
}
