package finalKeyword;

//The type B cannot subclass the final class A
public class B extends A
{
	
	int id;
	
	String name;
	
	public B() 
	{	
		
	}
	
	/*
	 * 
	 * Multiple markers at this line
	- Cannot override the final method 
	 from A
	- overrides finalKeyword.A.getId
	 */
	public void getId()
	{
		System.out.println(id);
	}
	
	public void getInfo()
	{
		super.getId();
		//System.out.println(super.id);
		System.out.println(name);
	}

	public static void main(String[] args) 
	{
		
		B b1 = new B();
		
	}

}

