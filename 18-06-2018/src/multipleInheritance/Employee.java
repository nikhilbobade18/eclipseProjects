package multipleInheritance;

public class Employee implements Employee2, Employee22
{
	int id;
	
	public Employee()
	{
		id = 5;
	}
	public void getId()
	{
		System.out.println(id);
	}
	
	public static void main(String[] args) 
	{
		
		
	}
	/*
		Multiple markers at this line
	- The return type is incompatible with 
	 Employee2.getInfo()
	- The return type is incompatible with 
	 Employee22.getInfo()
	- implements 
	 multipleInheritance.Employee2.getInfo
	 *
	 */
	public void getInfo()
	{
		// TODO Auto-generated method stub
		
	}
	
	public void get() 
	{
		// TODO Auto-generated method stub
		
	}
	

}
