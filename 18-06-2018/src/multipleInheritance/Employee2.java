package multipleInheritance;

public interface Employee2 
{
	//The blank final field a1 may not have been initialized
	//int a1;
	
	public static final int a1 = 23;
	
	//Abstract methods do not specify a body
	public abstract void getInfo();
	
}
