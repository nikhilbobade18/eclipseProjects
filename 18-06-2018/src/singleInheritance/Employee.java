
package singleInheritance;

public class Employee extends Employee1
{
	int id;
	public Employee()
	{
		id = 5;
	}
	
	
	public void getId()
	{
		System.out.println(id);
	}
	
	public int getId(int a1)
	{
		System.out.println(id);
		return 0;
	}
	
	public double getId(int a2, int a3)
	{
		
		System.out.println(id);
		return 0.0;
	}
	
	
	
	public static void main(String[] args) 
	{
		
		Employee e1 = new Employee();
		e1.get(12);
	}
	
	public void get(int a12) 
	{
		a12 = 56;
		int b = 34;
		System.out.println(b);
		System.out.println(id);
		System.out.println(a);
		System.out.println(a12);
		
	}
	
}
