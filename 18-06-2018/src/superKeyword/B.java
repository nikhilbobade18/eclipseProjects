package superKeyword;

public class B extends A
{
	
	int id;
	
	String name;
	
	public B() 
	{
		id = 1234;
		name = "Sanjay";
		//System.out.println("sub class");
	}
	
	public void getId()
	{
		System.out.println(id);
	}
	
	public void getInfo()
	{
		super.getId();
		//System.out.println(super.id);
		System.out.println(name);
	}

	public static void main(String[] args) 
	{
		
		
		B b1 = new B();
		b1.getInfo();
		
	}

}
