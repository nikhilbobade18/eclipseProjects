package superMethod;

public class B extends A
{
	
	int id;
	
	String name;
	
	public B() 
	{	
		//Constructor call must be the first statement in a constructor
		super(70, "annya");
		id = 1234;
		name = "Sanjay";
		
		System.out.println("B class");
	}
	
	public void getId()
	{
		System.out.println(id);
	}
	
	public void getInfo()
	{
		super.getId();
		//System.out.println(super.id);
		System.out.println(name);
	}

	public static void main(String[] args) 
	{
		
		B b1 = new B();
		
	}

}

