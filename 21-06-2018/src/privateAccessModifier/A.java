package privateAccessModifier;


public class A
{//super class

	//private int age;
	
	protected int age;
	
	public A() 
	{
		age = 25;
	}
	
	//The method getAge() from the type A is never used locally
	
	void getAge1()
	{
		System.out.println(age);
	}
	
	public static void main(String[] args) 
	{
		A v1 = new A();
		//The method getAge() from the type A1 is not visible
		//v1.getAge();
	}
	
}
