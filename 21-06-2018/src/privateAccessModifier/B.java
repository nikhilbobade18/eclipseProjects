package privateAccessModifier;

public class B extends A
{
	//sub class
	
	String name;
	
	public B()
	{
		name = "Anil";
	}
	
	public void getInfo()
	{
		//The field A.age is not visible
		int id = 78;
		System.out.println(age);
		System.out.println(name);
	}

	public static void main(String[] args) 
	{
	
		//id cannot be resolved to a variable
		//System.out.println(id);
		B a1 = new B();
		//The method getAge() from the type A is not visible
		//a1.getAge();
		a1.getInfo();
		
		

	}

}
