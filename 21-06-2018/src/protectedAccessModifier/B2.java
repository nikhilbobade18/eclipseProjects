package protectedAccessModifier;

import privateAccessModifier.A;

public class B2 extends A
{
	//sub class
	
	String name;
	
	public B2()
	{
		name = "Anil";
	}
	
	public void getInfo()
	{
		//The field A.age is not visible
		System.out.println(age);
		System.out.println(name);
	}

	public static void main(String[] args) 
	{
	
		B2 a1 = new B2();
		a1.getInfo();
		//The method getAge1() from the type A is not visible
		//a1.getAge1();
		
		

	}

}
