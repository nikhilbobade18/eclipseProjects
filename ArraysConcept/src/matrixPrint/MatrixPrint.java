package matrixPrint;

public class MatrixPrint {

	
	public static void main(String[] args) {
		
		//creating two matrices  
	
		int c[][]=new int[3][3];
		
		c[0][0] = 1;
		c[0][1] = 0;
		c[0][2] = 0;
		c[1][0] = 0;
		c[1][1] = 1;
		c[1][2] = 0;
		c[2][0] = 0;
		c[2][1] = 0;
		c[2][2] = 1;
		   
		for(int i=0;i<3;i++){  
		
			for(int j=0;j<3;j++){
			
				System.out.print(c[i][j]+ " ");  //printing matrix element
				 
			}
			
			System.out.println();//new line    
		}
		
	
		      
		
	}
}
