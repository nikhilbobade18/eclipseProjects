package calculator;

import java.util.Scanner;

public class Calc {
	public static int a, b, c, a2, ch3;
	public static String ch2;

	public void add(int a, int b) {
		c = a + b;
		System.out.println("Addition is : " + c);
	}

	public void sub(int a, int b) {
		c = a - b;
		System.out.println("Substraction is : " + c);

	}

	public void mul(int a, int b) {
		c = a * b;
		System.out.println("Multipication is : " + c);

	}

	public void div(int a, int b) {
		c = a / b;
		System.out.println("Division is : " + c);

	}

	public static void continueFunction() {
		System.out.println("Still want to continue....?? (Y/N)");
		Scanner s1 = new Scanner(System.in);
		ch2 = s1.next();
		if (ch2.equals("Y") || ch2.equals("y")) {
			System.out.println("********CALCULATOR********");
			System.out.println("1. Addition");
			System.out.println("2. Substraction");
			System.out.println("3. Multiplication");
			System.out.println("4. Division");

			System.out.println("Enter Your choice : ");
			ch3 = s1.nextInt();

			System.out.println("Enter your number now : \n");
			a2 = s1.nextInt();

			if (ch3 == 1) {
				c = c + a2;
				System.out.println("result : " + c);
				continueFunction();
			} else if (ch3 == 2) {
				c = c - a2;
				System.out.println("result : " + c);
				continueFunction();
			} else if (ch3 == 3) {
				c = c * a2;
				System.out.println("result : " + c);
				continueFunction();
			} else if (ch3 == 4) {
				c = c / a2;
				System.out.println("result : " + c);
				continueFunction();
			} else {
				System.out.println("Invalid Choice");
				continueFunction();
			}

		} else if (ch2.equals("N") || ch2.equals("n")) {
			System.out.println("Thank you for using calculator..");
		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int ch;

		System.out.println("********CALCULATOR********");
		System.out.println("1. Addition");
		System.out.println("2. Substraction");
		System.out.println("3. Multiplication");
		System.out.println("4. Division");

		System.out.println("Enter Your choice : ");

		Scanner s2 = new Scanner(System.in);
		ch = s2.nextInt();

		System.out.println("Enter two numbers now : \n");

		a = s2.nextInt();
		b = s2.nextInt();

		Calc ca = new Calc();

		switch (ch) {

		case 1:
			ca.add(a, b);
			break;

		case 2:
			ca.sub(a, b);
			break;

		case 3:
			ca.mul(a, b);
			break;

		case 4:
			ca.div(a, b);
			break;

		}

		ca.continueFunction();

	}

}
