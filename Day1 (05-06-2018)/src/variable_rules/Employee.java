package variable_rules;

public class Employee
{

	public static void main(String[] args)
	{
		
		//variables
		
		/*
		 * 1. a-z/A-Z, 0-9, !,@,#,$.....etc
		 * 2. age. id, name, cid, userName, sname...etc.
		 * 3. 
		 * 
		 * 
		 */
		
		int age = 26;
		
		float int1 = 23.45f;
		//Syntax error on token "int", invalid VariableDeclaratorId
		
		//int 12age = 26;
		
		//1. variable declaration not start with digits(0-9)
		
		//int @!age = 26;
		
		// 2. variable declaration not start special characters
		
		
		/*float marks = 70.23f;//Type mismatch: cannot convert from double to float
		
		char abc1[] = { 'A', 'A','A','A','A','A'};
		
		
		String abc = "name";
		
		char grade = 'A';
		
		char rank = '1';*/
		
		//Type mismatch: cannot convert from String to char
		//int age
		
		//age = 26
		
		//initializing/assigning value to the variable
		
		
		
		
		/*
		 * int is keyword/data type
		 * 
		 * age is variable
		 * 
		 * 26 is an integer constant value
		 * 
		 * Note:
		 * 
		 * int age => variable declaration
		 * 
		 * age = 26 => variable initialization
		 * 
		 */
		
		

	}

}
