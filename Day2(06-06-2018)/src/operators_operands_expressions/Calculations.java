package operators_operands_expressions;

public class Calculations 
{
	
//  access modifier non-access modifier return type method/function name(parameters)
	public static void simpleInterest()
	{// start
		
		//method body
		
		//functionality/business logic/ operatation
		double si = 0.0;
		
		int p = 4500, t = 3;
		
		float r = 2.5f;
		
		si = (p*t*r)/100;
		
		System.out.println("The simple interest is: "+si);
		
	
	}//end

	public static void main(String[] args) 
	{
		
		simpleInterest();

	}

}
