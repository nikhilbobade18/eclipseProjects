package jdbcConnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class DbConnection {
	
	public static void main(String[] args) {
		
		try {
			
			//step 1 : registering the jdbc driver
			Class.forName("com.mysql.jdbc.Driver");
			
			//step 2 : create connection
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/employer", "root", "admin");
			
			//step 3 : create statement
			Statement stm = con.createStatement();
			
			//String sql = "select * from information";
			
			//String sql = "insert into employee values('anil kapoor', 69500, 'awg', 'it', 5849572)";
			
			
			String sql = "delete from employee where name = 'anil kapoor'";
			
			
			//step 4 : execute query
			int i = stm.executeUpdate(sql);
			System.out.println(i + " row affected succesfully");
			
			
			//step 5 : close the connection
			con.close();
			
			
			
		}catch(Exception e)
		
		
		{
			System.out.println(e);
		}
		
	}

}
