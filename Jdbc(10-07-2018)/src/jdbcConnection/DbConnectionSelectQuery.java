package jdbcConnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class DbConnectionSelectQuery {
	
	public static void main(String[] args) {
		
		try {
			
			//step 1 : registering the jdbc driver
			Class.forName("com.mysql.jdbc.Driver");
			
			//step 2 : create connection
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/employer", "root", "admin");
			
			//step 3 : create statement
			Statement stm = con.createStatement();
			
			String sql = "select * from employee";
			
			
			//step 4 : execute query
	
			ResultSet rs = stm.executeQuery(sql);
			
			while(rs.next()) {
				System.out.println(rs.getString(1)+"  "+rs.getFloat(2)+"  "+rs.getString(3)+"  "+rs.getString(4)+"  "+rs.getString(5));
			}
			
			//step 5 : close the connection
			con.close();
			
		}catch(Exception e)
		
		
		{
			System.out.println(e);
		}
		
	}

}
