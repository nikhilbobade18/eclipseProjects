package jdbcConnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class DbConnectionUpdate {
	
	public static void main(String[] args) {
		
		try {
			
			//step 1 : registering the jdbc driver
			Class.forName("com.mysql.jdbc.Driver");
			
			//step 2 : create connection
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/employer", "root", "admin");
			
			//step 3 : create statement
			Statement stm = con.createStatement();
		
			
			String sql = "update employee set name = 'rakesh' where name = 'nikhil'";
			
			
			//step 4 : execute query
			int i = stm.executeUpdate(sql);
			System.out.println(i + " row affected succesfully");
			
			
			//step 5 : close the connection
			con.close();
			
			
			
		}catch(Exception e)
		
		
		{
			System.out.println(e);
		}
		
	}

}
