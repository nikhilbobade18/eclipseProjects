package typesOfConstructor;

public class Constructor {

	int a ;
	
	public Constructor(){
	
		a = 10;
		
	}
	
	public Constructor(int b){
		
		
	}
	
	public void fun1(){
		
		System.out.println(a);
		
	}
	
	
	
	public static void main(String[] args) {
		
		Constructor c = new Constructor();
		Constructor c2 = new Constructor();
		Constructor c3 = new Constructor();
		Constructor c4 = new Constructor();
		
		c.fun1();
		c2.fun1();
		c3.fun1();
		c4.fun1();
		
		
	}
	
}
