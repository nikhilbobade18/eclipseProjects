package typesOfConstructor;

public class Constructor2 {

	int a ;
	
	public Constructor2(){
	
		a = 10;
		
	}
	
	public Constructor2(int b){
		
		a = b;
		
	}
	
	public void fun1(){
		
		System.out.println(a);
		
	}
	
	
	
	public static void main(String[] args) {
		
		Constructor2 c = new Constructor2(10);
		Constructor2 c2 = new Constructor2(20);
		Constructor2 c3 = new Constructor2(30);
		Constructor2 c4 = new Constructor2(40);
		
		c.fun1();
		c2.fun1();
		c3.fun1();
		c4.fun1();
		
		
	}
	
}
