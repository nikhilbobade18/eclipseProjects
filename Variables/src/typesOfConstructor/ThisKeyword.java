package typesOfConstructor;

public class ThisKeyword {

	int a ;
	
	public ThisKeyword(){
	
		a = 10;
		
	}
	
	public ThisKeyword(int a){
		
		this.a = a;
		
	}
	
	public void fun1(){
		
		System.out.println(a);
		
	}
	
	
	
	public static void main(String[] args) {
		
		ThisKeyword c = new ThisKeyword(20);
		ThisKeyword c2 = new ThisKeyword(22);
		ThisKeyword c3 = new ThisKeyword(34);
		ThisKeyword c4 = new ThisKeyword(55);
		
		c.fun1();
		c2.fun1();
		c3.fun1();
		c4.fun1();
		
		
	}
	
}
