package typesOfConstructor;

public class ThisMethod {

	int a ;
	
	public ThisMethod(){
	
		
		this(100);
		a = 10;
		
		System.out.println("Default");
		
	}
	
	public ThisMethod(int b){
		
		//this();	//default
		System.out.println("Parameter");
		
	}
	
	public void fun1(){
		
		System.out.println(a);
		
	}
	
	
	
	public static void main(String[] args) {
		
		ThisMethod c = new ThisMethod();
		
		
	}
	
}
